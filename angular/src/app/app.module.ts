import { AccountConfigModule } from '@abp/ng.account/config';
import { CoreModule } from '@abp/ng.core';
import { registerLocale } from '@abp/ng.core/locale';
import { IdentityConfigModule } from '@abp/ng.identity/config';
import { SettingManagementConfigModule } from '@abp/ng.setting-management/config';
import { TenantManagementConfigModule } from '@abp/ng.tenant-management/config';
import { ThemeBasicModule } from '@abp/ng.theme.basic';
import { ThemeSharedModule } from '@abp/ng.theme.shared';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { APP_ROUTE_PROVIDER } from './route.provider';
import { HeaderComponent } from './header/header.component';
import { HorizontalComponent } from './horizontal/horizontal.component';
import { SigninComponent } from './signin/signin.component';
import { Routes, RouterModule } from '@angular/router';
import { LognoutComponent } from './lognout/lognout.component';
import { TabComponent } from './tab/tab.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MyrequestComponent } from './myrequest/myrequest.component';
import { ServicesComponent } from './services/services.component';
import { OperatingComponent } from './operating/operating.component';
import { ReportComponent } from './report/report.component';
import { HelpComponent } from './help/help.component';

const appRoutes: Routes =[
  {path: 'signin', component: SigninComponent},
  {path: 'signout', component: LognoutComponent},
  {path: 'services', component: ServicesComponent},
  {path: 'myrequest', component: MyrequestComponent},
  {path: 'operating', component: OperatingComponent},
  {path: 'report', component: ReportComponent},
  {path: 'help', component: HelpComponent}
];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule.forRoot({
      environment,
      registerLocaleFn: registerLocale(),
    }),
    ThemeSharedModule.forRoot(),
    AccountConfigModule.forRoot(),
    IdentityConfigModule.forRoot(),
    TenantManagementConfigModule.forRoot(),
    SettingManagementConfigModule.forRoot(),
    NgxsModule.forRoot(),
    ThemeBasicModule.forRoot(),
  ],
  declarations: [AppComponent, HeaderComponent, HorizontalComponent, SigninComponent, LognoutComponent, TabComponent, MyrequestComponent, ServicesComponent, OperatingComponent, ReportComponent, HelpComponent],
  providers: [APP_ROUTE_PROVIDER],
  bootstrap: [AppComponent],


})
export class AppModule {}
