import { Component, ViewChild} from '@angular/core';
import { TabComponent } from './tab/tab.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {

  Menu = [
    ['Menu 1', 0],
    ['Menu 2', 0],
    ['Menu 3', 0],
    ['Menu 4', 0],
    ['Menu 5', 0],
    ['Menu 6', 0],
    ['Menu 7', 0],
    ['Menu 8', 0],
  ];
@ViewChild(TabComponent)
  mytab: TabComponent;

OpentTab(x: any)
{
  this.mytab.addNewTab();
  for(let i=0; i<this.Menu.length;i++)
  {
    this.Menu[i][1]=0;
  }
}


}
